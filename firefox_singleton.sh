#!zsh
#create custom_shortcuts > add this `zsh ~/bin/firefox_singleton.sh Somx`
set -x

res=`xdotool search --name "Profile $1" | wc -l`
if [ $res -gt 0 ]
then
    xdotool search --name "Profile $1" windowactivate
else
    nohup firefox -P $1 &
fi
